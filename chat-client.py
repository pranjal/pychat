import socket, selectors, threading
from time import sleep
from tkinter import *
from tkinter.ttk import *

def msg_configure(event):
    event.widget["width"] = window.winfo_width()
    event.widget["height"] = window.winfo_height() - msg.winfo_height() - send.winfo_height() - connected.winfo_height()

def select_all(event):
    event.widget.select_range(0, END)
    event.widget.icursor(END)
    return "break"

def recvmsg(conn, lock):
    selector = selectors.SelectSelector()
    selector.register(conn, selectors.EVENT_READ)
    while not lock.locked():
        if selector.select(0):
            header = conn.recv(1)   
            if not header:
                conn.close()
                connected_nicks.set("Disconnected")
                break
            elif header[0] == 0:
                size = conn.recv(1)[0]
                recv = b""
                while len(recv) < size:
                    recv += conn.recv(size - len(recv))

                connected_nicks.set(recv.decode("utf-8"))
            else:
                recv = b""
                while len(recv) < header[0]:
                    recv += conn.recv(header[0] - len(recv))
                text_insert(msg_frame, recv.decode("utf-8"))
        sleep(0.1)
    else:
        conn.close()
    selector.close()


def text_insert(widget, text):
    widget["state"] = NORMAL
    widget.insert(END, text)
    widget["state"] = DISABLED
    msg_frame.yview_scroll(1, "units")

def sendmsg(arg):
    if msg.get() and conn.fileno() != -1:
        send = f"{nick}: {msg.get()}\n"
        text_insert(msg_frame, send)
        msg.delete(0, END)
        header = bytes((len(send),))
        conn.sendall(header + send.encode("utf-8"))

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
nick = input("Enter nick: ")
conn.connect((input("Enter IP: "), 9999))
conn.sendall(bytes((len(nick),)) + nick.encode("utf-8"))
selector = selectors.SelectSelector()
selector.register(conn, selectors.EVENT_READ)
lock = threading.Lock()

window = Tk()
window.title("Chat Client")

window.rowconfigure(1, weight=1)

msg_frame = Text(window, wrap="word", width=window.winfo_width())
msg = Entry(window)
send = Button(window, text="Send message")
connected_nicks = StringVar()
connected = Label(window, textvariable=connected_nicks)

connected.grid(row=0, column=0, sticky="w")
msg_frame.grid(row=1, column=0, sticky="sw")
msg.grid(row=2, column=0, sticky="w")
send.grid(row=3, column=0, sticky="w")

msg.bind("<Return>", sendmsg)
msg.bind("<Control-a>", select_all)
send.bind("<Button-1>", sendmsg)
msg_frame.bind("<Configure>", msg_configure)

thr = threading.Thread(target=recvmsg, args=(conn, lock))
thr.start()
window.mainloop()
lock.acquire()
