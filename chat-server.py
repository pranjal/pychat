import socket, selectors, threading
from time import sleep
from tkinter import *
from tkinter.ttk import *

def text_insert(widget, text):
    widget["state"] = NORMAL
    widget.insert(END, text)
    widget["state"] = DISABLED
    msg_frame.yview_scroll(1, "units")

def msg_configure(event):
    event.widget["width"] = window.winfo_width()
    event.widget["height"] = window.winfo_height() - msg.winfo_height() - send_btn.winfo_height() - connected.winfo_height()

def select_all(event):
    event.widget.select_range(0, END)
    event.widget.icursor(END)
    return "break"

def sendmsg(send, conn_nosend=None):
    text_insert(msg_frame, send)
    header = bytes((len(send),))

    for conn in conns:
        if conn != conn_nosend:
            conn.sendall(header + send.encode("utf-8"))

def sendmsg_callback(arg):
    if msg.get():
        sendmsg(f"{nick}: {msg.get()}\n")
        msg.delete(0, END)

def conn_thread(s, conns, lock):
    selector = selectors.SelectSelector()
    while not lock.locked():
        try:
            conns.append(s.accept()[0])
            selector.register(conns[-1], selectors.EVENT_READ)
            size = conns[-1].recv(1)[0]
            nick = b""
            while len(nick) < size:
                nick += conns[-1].recv(size - len(nick))
            nicks.append(nick.decode("utf-8"))
            connected_nicks.set(connected_nicks.get() + ", " +  nicks[-1])

            for conn in conns:
                send = connected_nicks.get().encode("utf-8")
                header = bytes((0, len(send)))
                conn.sendall(header + send)
        except: pass

        while True:
            try:
                selection = [i[0].fileobj for i in selector.select(0)]
                assert(selection)
            except: break
            for conn in selection:
                header = conn.recv(1)
                if header:
                    recv = b""
                    while len(recv) < header[0]:
                        recv += conn.recv(header[0]-len(recv))
                    sendmsg(recv.decode("utf-8"), conn)
                else:
                    conn.close()
                    selector.unregister(conn)
                    i = conns.index(conn)
                    del nicks[i+1]
                    del conns[i]
                    connected_nicks.set("Connected: " + ", ".join(nicks))
                    for conn in conns:
                        send = connected_nicks.get().encode("utf-8")
                        header = bytes((0, len(send)))
                        conn.sendall(header + send)
        sleep(0.1)
    s.close()
    for conn in conns:
        conn.close()
    selector.close()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setblocking(0)

s.bind(('', 9999))
s.listen(0)

conns = []
lock = threading.Lock()
nick = input("Enter nick: ")

conn_thr = threading.Thread(target=conn_thread, args=(s, conns, lock))
conn_thr.start()

window = Tk()

window.title("Chat Server")
window.rowconfigure(1, weight=1)

nicks = [nick]
connected_nicks = StringVar()
connected_nicks.set(f"Connected: {nick}")
connected = Label(window, textvariable=connected_nicks)

msg_frame = Text(window, wrap="word", state=DISABLED)
msg = Entry(window)
send_btn = Button(window, text="Send message")

connected.grid(row=0, column=0, sticky="w")
msg_frame.grid(row=1, column=0, sticky="sw")
msg.grid(row=2, column=0, sticky="w")
send_btn.grid(row=3, column=0, sticky="w")

msg.bind("<Return>", sendmsg_callback)
msg.bind("<Control-a>", select_all)
send_btn.bind("<Button-1>", sendmsg_callback)
msg_frame.bind("<Configure>", msg_configure)

window.mainloop()
lock.acquire()
